import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
const Layout = () => import('@/layout')
const Login = () => import('@/views/login/index')
const Error = () => import('@/views/404')
const Tree = () => import('@/views/tree/index')
const Dashboard = () => import('@/views/dashboard/index')
const Contentlist = () => import('@/views/content/contentList')
const jurisdictionManage = () => import('@/views/jurisdiction/jurisdictionManage')
export const constantRoutes = [
  {
    path: '/login',
    component: Login,
    hidden: true,
    name: 'login'
  },

  {
    path: '/404',
    component: Error,
    hidden: true,
    name: 'error'
  },

  {
    path: '/',
    component: Layout,
    name: Layout,
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: Dashboard,
      meta: { title: '大数据看板', icon: 'basedata' }
    }]
  },

  {
    path: '/content',
    component: Layout,
    redirect: '/content/table',
    name: 'Content',
    meta: { title: '内容管理', icon: 'content' },
    children: [
      {
        path: 'table',
        name: 'Table',
        component: () => import('@/views/table/index'),
        meta: { title: '内容分类' }
      },
      {
        path: 'contentlist',
        name: 'Contentlist',
        component: Contentlist,
        meta: { title: '内容列表' }
      },
      {
        path: 'tree',
        name: 'Tree',
        component: Tree,
        meta: { title: '树结构' }
      }
    ]
  },
  {
    path: '/jurisdiction',
    component: Layout,
    redirect: '/jurisdiction/jurisdctionmanage',
    name: 'Jurisdiction',
    meta: { title: '权限管理', icon: 'content' },
    children: [
      {
        path: 'jurisdictionmanage',
        name: 'jurisdictionManage',
        component: jurisdictionManage,
        meta: { title: '权限管理' }
      }
    ]
  },
  {
    path: '/form',
    component: Layout,
    children: [
      {
        path: 'index',
        name: 'Form',
        component: () => import('@/views/form/index'),
        meta: { title: 'Form', icon: 'form' }
      }
    ]
  },

  {
    path: '/nested',
    component: Layout,
    redirect: '/nested/menu1',
    name: 'Nested',
    meta: {
      title: 'Nested',
      icon: 'nested'
    },
    children: [
      {
        path: 'menu1',
        component: () => import('@/views/nested/menu1/index'), // Parent router-view
        name: 'Menu1',
        meta: { title: 'Menu1' },
        children: [
          {
            path: 'menu1-1',
            component: () => import('@/views/nested/menu1/menu1-1'),
            name: 'Menu1-1',
            meta: { title: 'Menu1-1' }
          },
          {
            path: 'menu1-2',
            component: () => import('@/views/nested/menu1/menu1-2'),
            name: 'Menu1-2',
            meta: { title: 'Menu1-2' },
            children: [
              {
                path: 'menu1-2-1',
                component: () => import('@/views/nested/menu1/menu1-2/menu1-2-1'),
                name: 'Menu1-2-1',
                meta: { title: 'Menu1-2-1' }
              },
              {
                path: 'menu1-2-2',
                component: () => import('@/views/nested/menu1/menu1-2/menu1-2-2'),
                name: 'Menu1-2-2',
                meta: { title: 'Menu1-2-2' }
              }
            ]
          },
          {
            path: 'menu1-3',
            component: () => import('@/views/nested/menu1/menu1-3'),
            name: 'Menu1-3',
            meta: { title: 'Menu1-3' }
          }
        ]
      },
      {
        path: 'menu2',
        component: () => import('@/views/nested/menu2/index'),
        meta: { title: 'menu2' }
      }
    ]
  },

  {
    path: 'external-link',
    component: Layout,
    children: [
      {
        path: 'https://panjiachen.github.io/vue-element-admin-site/#/',
        meta: { title: 'External Link', icon: 'link' }
      }
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
